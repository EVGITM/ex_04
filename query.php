<?php
//EVGENI TEMNIKOV 321775272

class Query{
    protected $dbc;
    function __construct($dbc){
        $this->dbc = $dbc;
    }

    // Perform query
    public function query($query){
        if ($result = $this->dbc-> query($query)){
            return $result;
        }
        else{
            echo "Something went wrong with the query";
        }
    }
}
?>